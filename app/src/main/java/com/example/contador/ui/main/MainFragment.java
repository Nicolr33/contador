package com.example.contador.ui.main;

import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.contador.R;

public class MainFragment extends Fragment {


    private MainViewModel mViewModel;
    private int p1vida = 20;
    private int p2vida = 20;
    private int p1veneno;
    private int p2veneno;

    private ImageView p1visumar;
    private ImageView p1virestar;
    private Button p1vsumar;
    private Button p1vrestar;
    private ImageView p2visumar;
    private ImageView p2virestar;
    private Button p2vsumar;
    private Button p2vrestar;
    private TextView p1contadorv;
    private TextView p2contadorv;
    private AlertDialog.Builder alert;

    private Button reset;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);

        p1visumar =  view.findViewById(R.id.p1visumar);
        p1virestar =  view.findViewById(R.id.p1virestar);
        p1vsumar = view.findViewById(R.id.p1vsumar);
        p1vrestar = view.findViewById(R.id.p1vrestar);

        p2visumar =  view.findViewById(R.id.p2visumar);
        p2virestar =  view.findViewById(R.id.p2virestar);
        p2vsumar = view.findViewById(R.id.p2vsumar);
        p2vrestar = view.findViewById(R.id.p2vrestar);

        p1contadorv = view.findViewById(R.id.contador1);
        p2contadorv = view.findViewById(R.id.contador2);
        reset = view.findViewById(R.id.reset);

        alert = new AlertDialog.Builder(getActivity());




        // Player one

        p1visumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p1vida++;
                p1contadorv.setText(String.format("%d/%d", p1vida, p1veneno));
            }
        });

        p1virestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (p1vida <= 0) {
                    alert.setTitle("Ha perdido el jugador 1");
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            //Your action here
                        }
                    });
                    alert.show();
                } else {
                    p1vida--;
                    p1contadorv.setText(String.format("%d/%d", p1vida, p1veneno));
                }
            }
        });

        p1vsumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p1veneno++;
                p1contadorv.setText(String.format("%d/%d", p1vida, p1veneno));
            }
        });

        p1vrestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p1veneno--;
                p1contadorv.setText(String.format("%d/%d", p1vida, p1veneno));
            }
        });


        // Player two

        p2visumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p2vida++;
                p2contadorv.setText(String.format("%d/%d", p2vida, p2veneno));
            }
        });

        p2virestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (p2vida != 0) {
                    p2vida--;
                    p2contadorv.setText(String.format("%d/%d", p2vida, p2veneno));
                } else {
                    alert.setTitle("Ha perdido el jugador 2");
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            //Your action here
                        }
                    });
                    alert.show();
                }


            }
        });

        p2vsumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p2veneno++;
                p2contadorv.setText(String.format("%d/%d", p2vida, p2veneno));
            }
        });

        p2vrestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (p2veneno != 0) {

                    p2veneno--;
                    p2contadorv.setText(String.format("%d/%d", p2vida, p2veneno));
                }

            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restore();
            }
        });


        return view;
    }

    private void restore() {
        this.p1vida = 20;
        this.p2vida = 20;
        this.p1veneno = 0;
        this.p2veneno = 0;

        updateCounters();
    }

    private void updateCounters() {
        p1contadorv.setText(String.format("%d/%d", p1vida, p1veneno));
        p2contadorv.setText(String.format("%d/%d", p2vida, p2veneno));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

}
